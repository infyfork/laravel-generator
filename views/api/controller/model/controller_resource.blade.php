@php
    echo "<?php".PHP_EOL;
@endphp

namespace {{ $config->namespaces->apiController }};

// Framework classes
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

// Vendor classes

// Application classes
use {{ $config->namespaces->app }}\Http\Controllers\AppBaseController;
use {{ $config->namespaces->apiRequest }}\Create{{ $config->modelNames->name }}APIRequest;
use {{ $config->namespaces->apiRequest }}\Update{{ $config->modelNames->name }}APIRequest;

use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};
use {{ $config->namespaces->apiResource }}\{{ $config->modelNames->name }}Resource;

{!! $docController !!}
class {{ $config->modelNames->name }}APIController extends AppBaseController
{
    {!! $docIndex !!}
    public function index(Request $request): JsonResponse
    {
        $query = {{ $config->modelNames->name }}::query();

        return {{ $config->modelNames->name }}Resource::collection($query->paginate())->additional([
@if($config->options->localized)
            'message' => __('messages.retrieved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->humanPlural }} retrieved successfully',
@endif
        ]);
    }

    {!! $docStore !!}
    public function store(Create{{ $config->modelNames->name }}APIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var {{ $config->modelNames->name }} ${{ $config->modelNames->camel }} */
        ${{ $config->modelNames->camel }} = {{ $config->modelNames->name }}::create($input);

        return (new {{ $config->modelNames->name }}Resource(${{ $config->modelNames->camel }}))->additional([
@if($config->options->localized)
            'message' => __('messages.saved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} saved successfully',
@endif
        ]);
    }

    {!! $docShow !!}
    public function show($id): JsonResponse
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        return (new {{ $config->modelNames->name }}Resource(${{ $config->modelNames->camel }}))->additional([
@if($config->options->localized)
            'message' => __('messages.retrieved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} retrieved successfully',
@endif
        ]);
    }

    {!! $docUpdate !!}
    public function update($id, Update{{ $config->modelNames->name }}APIRequest $request): JsonResponse
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        ${{ $config->modelNames->camel }}->fill($request->all());
        ${{ $config->modelNames->camel }}->save();

        return (new {{ $config->modelNames->name }}Resource(${{ $config->modelNames->camel }}))->additional([
@if($config->options->localized)
            'message' => __('messages.updated', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} updated successfully',
@endif
        ]);
    }

    {!! $docDestroy !!}
    public function destroy($id): JsonResponse
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        ${{ $config->modelNames->camel }}->delete();

        return (new JsonResource([compact('id')]))->additional([
@if($config->options->localized)
            'message' => __('messages.deleted', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} deleted successfully',
@endif
        ]);
    }

    protected function findOrFail($id): {{ $config->modelNames->name }}
    {
        return {{ $config->modelNames->name }}::findOr($id, function() {
@if($config->options->localized)
            $message = __('messages.not_found', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]);
@else
            $message = '{{ $config->modelNames->human }} not found';
@endif
            abort(404, $message);
        });
    }
}
