@php
    echo "<?php".PHP_EOL;
@endphp

namespace {{ $config->namespaces->controller }};

// Framework classes
use Illuminate\Http\Request;

// Vendor classes

// Application classes
use {{ $config->namespaces->app }}\Http\Controllers\AppBaseController;
use {{ $config->namespaces->request }}\Create{{ $config->modelNames->name }}Request;
use {{ $config->namespaces->request }}\Update{{ $config->modelNames->name }}Request;

use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};
@if(config('laravel_generator.tables') == 'datatables')
use {{ $config->namespaces->dataTables }}\{{ $config->modelNames->name }}DataTable;
@endif

class {{ $config->modelNames->name }}Controller extends AppBaseController
{
    /**
     * Display a listing of the {{ $config->modelNames->name }}.
     */
    {!! $indexMethod !!}

    /**
     * Show the form for creating a new {{ $config->modelNames->name }}.
     */
    public function create()
    {
        return view('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.create');
    }

    /**
     * Store a newly created {{ $config->modelNames->name }} in storage.
     */
    public function store(Create{{ $config->modelNames->name }}Request $request)
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }} = {{ $config->modelNames->name }}::create($input);

        return redirect(route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->camelPlural }}.index'))->withMessage([
            'type' => 'success',
@if($config->options->localized)
            'text' => __('messages.saved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'text' => '{{ $config->modelNames->human }} saved successfully',
@endif
        ]);
    }

    /**
     * Display the specified {{ $config->modelNames->name }}.
     */
    public function show($id)
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        return view('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.show')->with([
            '{{ $config->modelNames->camel }}' => ${{ $config->modelNames->camel }},
        ]);
    }

    /**
     * Show the form for editing the specified {{ $config->modelNames->name }}.
     */
    public function edit($id)
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        return view('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.edit')->with([
            '{{ $config->modelNames->camel }}' => ${{ $config->modelNames->camel }},
        ]);
    }

    /**
     * Update the specified {{ $config->modelNames->name }} in storage.
     */
    public function update($id, Update{{ $config->modelNames->name }}Request $request)
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        ${{ $config->modelNames->camel }}->fill($request->all());
        ${{ $config->modelNames->camel }}->save();

        return redirect(route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->camelPlural }}.index'))->withMessage([
            'type' => 'success',
@if($config->options->localized)
            'text' => __('messages.updated', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'text' => '{{ $config->modelNames->human }} updated successfully',
@endif
        ]);
    }

    /**
     * Remove the specified {{ $config->modelNames->name }} from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        ${{ $config->modelNames->camel }} = $this->findOrFail($id);

        ${{ $config->modelNames->camel }}->delete();

        return redirect(route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->camelPlural }}.index'))->withMessage([
            'type' => 'success',
@if($config->options->localized)
            'text' => __('messages.deleted', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'text' => '{{ $config->modelNames->human }} deleted successfully',
@endif
        ]);
    }

    protected function findOrFail($id): {{ $config->modelNames->name }}
    {
        return $this->{{ $config->modelNames->camel }}Repository->query()->findOr($id, function() {
@if($config->options->localized)
            $message = __('messages.not_found', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]);
@else
            $message = '{{ $config->modelNames->human }} not found';
@endif
            abort(404, $message);
        });
    }
}
